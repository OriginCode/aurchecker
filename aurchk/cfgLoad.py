import json
import asyncio
from pathlib import Path

from .genPkgsConf import genPkgsConf
from .pacmsg import pacmsg

# Parse config files.
async def cfgLoad():
    cfgPath = Path.home().joinpath(Path('.config/aurchk/config.json'))
    cfgParentPath = cfgPath.parents[0]
    
    if cfgPath.exists() and cfgPath.stat().st_size > 0:
        with open(cfgPath) as f:
            config = json.load(f)
    else:
        if not cfgParentPath.exists():
            pacmsg('Config not found.')
            Path.home().joinpath(Path('.config/aurchk/')).mkdir(parents=True)
        elif cfgPath.stat().st_size == 0:
            pacmsg('Config file is empty.')
            cfgPath.unlink()
        
        pacmsg('Generating default config...')
        cfgPath.touch()
        with open(cfgPath, 'w') as f:
            # Default config, if the default config file (path) does not exist, create one with the following config.
            config = {
                    'pkgListPath': str(Path.home().joinpath(Path('.config/aurchk/pkgs.json'))),
                    'pkgClonePath': str(Path.home().joinpath(Path('.cache/aurchk/')))
                    }
            f.write(json.dumps(config, indent=4))
    
    # Another config file for storing package names and their current versions. Generate if !exist.
    pkgListPath = Path(config['pkgListPath'])
    if not pkgListPath.exists() or pkgListPath.stat().st_size == 0:
        pacmsg('Generating pkgs.json from pacman...')
        genPkgsConf(pkgListPath)
        with open(pkgListPath) as f:
            oldVerDict = json.load(f)
    else:
        with open(pkgListPath) as f:
            oldVerDict = json.load(f)

    if not (pkgClonePath := Path(config['pkgClonePath'])).exists():
        pkgClonePath.mkdir(parents=True)
    
    return pkgListPath, pkgClonePath, oldVerDict