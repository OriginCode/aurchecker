from termcolor import colored

def pacmsg(string):
    print(colored('::', 'cyan', attrs=['bold']) + ' ' + colored(string, attrs=['bold']))